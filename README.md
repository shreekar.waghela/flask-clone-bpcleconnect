# BPCL eConnect Clone
This project is a clone of BPCL eConnect website.

This website is used by BPCL Petrol Pump dealers for logging complaints regarding any problem with the equipments in their premises. 

These complaints are accessed by the Authorized vendors provided by BPCL according to the category of equipment a vendor can fix.

## Features:
-- First Admin creates a Customer/Vendor-CODE that is used to register a user and login.
###
-- Then a Customer/Vendor uses the provided CODE to register themselves and login.
###
-- Customer/Vendor is redirected to their home page, where their details are shown.
###
-- Customer gets a page to register complaint and a page to view their active complaints.
###
-- Vendor gets a page to view the complaints that are assigned to them.
###
-- A search bar for searching complaints based on complaint category.
###
-- An option to download a txt file with complaint details for Customer/Vendor.

## Heroku deployment:
https://ecomplaint.herokuapp.com/login

## Technologies used:
-- Python(Flask)
###
-- HTML and CSS
###
-- Bootstrap
###
-- Celery and redis for file download (background process)
###
-- ElasticSearch for search bar

## Few images:
### Login page:
![login page](./login.png)
### Registration page:
![registration page](./registration.png)
### Home Page:
![home page](./customerhome.png)
### New Complaint Page:
![complaints page](./complaintpage.png)
### Active Complaints Page:
![active complaints page](./activecomplaints.png)
### Search Results:
![search result page](./searchresult.png)
