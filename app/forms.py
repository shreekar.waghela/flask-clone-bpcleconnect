from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from app.models import User
from flask import request

class LoginForm(FlaskForm):
    code = StringField('Partner Code', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Sign In')


class AddUser(FlaskForm):
    partner_type = SelectField('Partner Type', choices=[(
        'customer', 'CUSTOMER'), ('vendor', 'VENDOR')])
    code = StringField('Partner Code', validators=[DataRequired()])
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    phone = StringField('Phone Number', validators=[DataRequired()])
    category = SelectField('Category', choices=[('None', 'None'), ('building', 'BUILDING'), ('mpd', 'MPD'),
                                                ('tank', 'Tank'), ('canopy', 'CANOPY'),
                                                ('electrical', 'ELECTRICAL'),
                                                ('signages',
                                                 'SIGNAGES'), ('ups', 'UPS'),
                                                ('drive way', 'DRIVE WAY'),
                                                ('stp', 'STP'), ('atg',
                                                                 'ATG'), ('automation', 'AUTOMATION'),
                                                ('edc', 'EDC')])
    submit = SubmitField('Submit')

    def validate_code(self, code):
        user = User.query.filter_by(code=code.data).first()
        if user is not None:
            raise ValidationError('Please use a different code')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email')


class RegistrationForm(FlaskForm):
    partner_type = SelectField('Partner Type', choices=[(
        'customer', 'CUSTOMER'), ('vendor', 'VENDOR')])
    code = StringField('Parter Code', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[
                              DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_code(self, code):
        user = User.query.filter_by(code=code.data).first()
        if user is None:
            raise ValidationError(
                'Parter Code not available!! Please check with Admin!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError(
                'Email not available!! Please check with Admin!')


class NewComplaint(FlaskForm):
    category = SelectField('Category', choices=[('building', 'BUILDING'), ('mpd', 'MPD'),
                                                ('tank', 'Tank'), ('canopy', 'CANOPY'),
                                                ('electrical', 'ELECTRICAL'),
                                                ('signages',
                                                 'SIGNAGES'), ('ups', 'UPS'),
                                                ('drive way', 'DRIVE WAY'),
                                                ('stp', 'STP'), ('atg',
                                                                 'ATG'), ('automation', 'AUTOMATION'),
                                                ('edc', 'EDC')])
    priority = SelectField('Priority', choices=[('sales stopped', 'SALES STOPPED'),
                                                ('sales not stopped', 'SALES NOT STOPPED')])
    remarks = TextAreaField('Remarks', validators=[DataRequired()])
    submit = SubmitField('Submit')

class ResetPaswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Pasword Reset')

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')

class SearchForm(FlaskForm):
    q = StringField('Search', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
