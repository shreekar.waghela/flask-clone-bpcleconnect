from app import app, db
from flask import render_template, redirect, flash, url_for, g, request
from app.forms import LoginForm, AddUser, RegistrationForm, NewComplaint, ResetPaswordRequestForm, ResetPasswordForm, SearchForm
from app.models import User, Complaint
from flask_login import current_user, login_user, logout_user, login_required
from datetime import datetime, date
from app.email import send_password_reset_email
from app import celery
import os


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()


@app.route('/')
@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user.html', user=user)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        if current_user.partner_type == 'admin':
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('user', username=current_user.username))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(code=form.code.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user)
        if current_user.partner_type == 'admin':
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('user', username=current_user.username))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/admin', methods=['GET', 'POST'])
@login_required
def admin():
    form = AddUser()
    if form.validate_on_submit():
        user = User(partner_type=form.partner_type.data, code=form.code.data,
                    username=form.username.data, email=form.email.data, phone=form.phone.data, category=form.category.data)
        db.session.add(user)
        db.session.commit()
        flash("New user added!")
        return redirect(url_for('admin'))
    return render_template('admin.html', title='Admin', form=form)


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User.query.filter_by(code=form.code.data).first()
        user.set_password(form.password.data)
        db.session.commit()
        flash('Registration Successful!!')
        return redirect(url_for('login'))
    return render_template('registration.html', title='Registration', form=form)


@app.route('/newcomplaint', methods=['GET', 'POST'])
@login_required
def newcomplaint():
    form = NewComplaint()
    if form.validate_on_submit():
        complaint = Complaint(category=form.category.data, priority=form.priority.data,
                              remarks=form.remarks.data, user_id=current_user.id)
        db.session.add(complaint)
        db.session.commit()
        flash('New Complaint Registered!')
        return redirect('/newcomplaint')
    return render_template('newcomplaint.html', title='Add New Complaint', form=form)


@app.route('/activecomplaints', methods=['GET'])
@login_required
def activecomplaints():
    complaint_details_customer = Complaint.query.filter_by(
        user_id=current_user.id)
    complaint_details_vendor = Complaint.query.filter_by(
        category=current_user.category)
    user_details = User.query.all()
    return render_template('activecomplaints.html', complaint_details_customer=complaint_details_customer, complaint_details_vendor=complaint_details_vendor, user_details=user_details)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    form = ResetPaswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html', title='Reset Password', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('login'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset!')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)

#elastic search implementation
@app.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('activecomplaints'))
    page = request.args.get('page', 1, type=int)
    user_details = User.query.all()
    complaints, total = Complaint.search(
        g.search_form.q.data, page, app.config['POSTS_PER_PAGE'])
    complaints_customer = complaints.filter_by(user_id=current_user.id)
    complaints_vendor = complaints.filter_by(category=current_user.category)
    next_url = url_for('search', q=g.search_form.q.data, page=page + 1) \
        if total > page * app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title='Search', user_details=user_details,
                           complaints_customer=complaints_customer, complaints_vendor=complaints_vendor,
                           next_url=next_url, prev_url=prev_url)

#celery implementation
@app.route('/generate_file/<id>')
@login_required
def generate_file(id):
    task_id = create_complaint_log.delay(id)
    return {'id': str(task_id)}

@celery.task(name="tasks.create_complaint_log")
def create_complaint_log(id):
    current_user = User.query.get(id)
    complaints = Complaint.query.filter_by(user_id=current_user.id)

    complaint_details =""

    for complaint in complaints:
        complaint_details += f'''
        Complaint ID : {complaint.id}
        Category: {complaint.category}
        Priority: {complaint.priority}
        Remarks: {complaint.remarks}
        Date: {complaint.date}
        --------------------------------------------
        '''

    file_path = os.path.join(os.getcwd(),'app', 'static', '{}.txt'.format(current_user.id))
    if os.path.exists(file_path):
        os.remove(file_path)

    with open(file_path, 'w') as f:
        f.write(complaint_details)
    
    url = "/static/{}.txt".format(current_user.id)
    
    return url

@app.route('/get_file/<id>')
def get_file(id):
    task = celery.AsyncResult(id)
    url = task.result
    return {'url': url}
