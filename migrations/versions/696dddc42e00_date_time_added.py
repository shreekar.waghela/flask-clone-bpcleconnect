"""Date time added

Revision ID: 696dddc42e00
Revises: 69e53894f880
Create Date: 2019-09-26 11:21:52.735148

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '696dddc42e00'
down_revision = '69e53894f880'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('ix_admin_email', table_name='admin')
    op.drop_index('ix_admin_username', table_name='admin')
    op.drop_table('admin')
    op.add_column('user', sa.Column('last_seen', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'last_seen')
    op.create_table('admin',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('username', sa.VARCHAR(length=64), nullable=True),
    sa.Column('email', sa.VARCHAR(length=120), nullable=True),
    sa.Column('password_hash', sa.VARCHAR(length=128), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_admin_username', 'admin', ['username'], unique=1)
    op.create_index('ix_admin_email', 'admin', ['email'], unique=1)
    # ### end Alembic commands ###
