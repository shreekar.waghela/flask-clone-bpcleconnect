FROM ubuntu:latest
LABEL Shreekar Waghela="wshree@gmail.com"
RUN apt-get update -y
RUN apt-get install python3 -y
RUN apt-get install -y python3-pip -y
RUN apt-get install build-essential libssl-dev libffi-dev python3-dev -y
RUN apt-get install sqlite3 -y
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["econnect.py"]

